const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const routes = require('./routes/routes');
const config = require('./config');
const app = express();
const { populateAdmins, populateUsers } = require('./services/populate');

mongoose.Promise = global.Promise;
if (process.env.NODE_ENV !== 'test') {
  mongoose.connect(config.mongoUrl);
}

// Populate db
populateAdmins();
populateUsers();

app.use(bodyParser.json({
  limit: config.bodyLimit
}));
routes(app);

app.use((err, req, res, next) => {
  res.status(422).send({error: err.message});
});

const server = http.createServer(app);

module.exports = server;
