const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

const AdminSchema = new Schema({
  username: {
    type: String,
    unique: true
  },
  password: String
});

AdminSchema.pre('save', function(next) {
  const admin = this;

  bcrypt.genSalt(10, function(err, salt) {
    if (err) { return next(err); }

    bcrypt.hash(admin.password, salt, null, function(err, hash) {
      if (err) { return next(err); }

      admin.password = hash;
      next();
    })
  })
})

AdminSchema.methods.comparePassword = function(candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) { return callback(err); }

    callback(null, isMatch);
  });
};

const Admin = mongoose.model('admin', AdminSchema);
module.exports = Admin;
