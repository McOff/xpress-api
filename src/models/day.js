const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const WorkoutSchema = new Schema({
  start: Number,
  workout: {
    type: Schema.Types.ObjectId,
    ref: 'workout'
  },
  instructor: {
    type: Schema.Types.ObjectId,
    ref: 'instructor'
  },
  price: Number,
  notes: String
});

const DaySchema = new Schema({
  date: String,
  workouts: [WorkoutSchema]
});

const Day = mongoose.model('day', DaySchema);
module.exports = Day;
