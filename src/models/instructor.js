const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const InstructorSchema = new Schema({
  firstname: String,
  lastname: String
});

const Instructor = mongoose.model('instructor', InstructorSchema);
module.exports = Instructor;
