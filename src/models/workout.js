const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const WorkoutSchema = new Schema({
  duration: {
    type: Number,
    default: 55
  },
  title: String
});

const Workout = mongoose.model('workout', WorkoutSchema);
module.exports = Workout; 
