const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

const ClassSchema = new Schema({
  date: Number,
  title: String,
  visited: {
    type: Boolean,
    default: true
  }
});

const UserSchema = new Schema({
  cardNumber: {
    type: String,
    unique: true
  },
  firstname: String,
  lastname: String,
  password: String,
  classes: [ClassSchema]
});

UserSchema.pre('save', function(next) {
  const user = this;

  bcrypt.genSalt(10, function(err, salt) {
    if (err) { return next(err); }

    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) { return next(err); }

      user.password = hash;
      next();
    })
  })
});


UserSchema.methods.comparePassword = function(candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) { return callback(err); }

    callback(null, isMatch);
  });
};

const User = mongoose.model('user', UserSchema);
module.exports = User;
