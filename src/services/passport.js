const passport = require('passport');
const User = require('../models/user');
const Admin = require('../models/admin');
const config = require('../config');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');


// User local strategy
const localUserOptions = { usernameField: 'cardNumber' };
const localUserLogin = new LocalStrategy(localUserOptions, function(cardNumber, password, done) {

  User.findOne({ cardNumber }, (err, user) => {

    if (err) { return done(err); }
    if (!user) { return done(null, false); }

    user.comparePassword(password, function(err, isMatch) {
      if (err) { return done(err); }
      if (!isMatch) { return done(null, false); }

      return done(null, user);
    })
  })
})

// Admin local strategy
const localAdminLogin = new LocalStrategy( function(username, password, done) {
  Admin.findOne({ username }, (err, admin) => {
    if (err) { return done(err); }
    if (!admin) { return done(null, false); }

    admin.comparePassword(password, function(err, isMatch) {
      if (err) { return done(err); }
      if (!isMatch) { return done(null, false); }

      return done(null, admin);
    })
  })
});

// Create JWT Strategy

const jwtOptionsAdmin = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: config.secretAdmin
};

const jwtLoginAdmin = new JwtStrategy(jwtOptionsAdmin, (payload, done) => {
  Admin.findById(payload.sub, (err, admin) => {
    if (err) { return done(err, false); }

    if (admin) {
      done(null, admin);
    } else {
      done(null, false);
    }
  })
});

const jwtOptionsUser = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: config.secretUser
};
const jwtLoginUser = new JwtStrategy(jwtOptionsUser, (payload, done) => {
  User.findById(payload.sub, (err, user) => {
    if (err) { return done(err, false); }

    if (user) {
      done(null, user);
    } else {
      done(null, false);
    }
  })
});

// Tell passport to use this strategy
passport.use('jwtAdmin', jwtLoginAdmin);
passport.use('jwtUser', jwtLoginUser);
passport.use('localAdmin', localAdminLogin);
passport.use('localUser', localUserLogin);
