const User = require('../models/user');
const Admin = require('../models/admin');
const { adminName, adminPassword, userCardNumber, userPassword } = require('../config');

module.exports = {
  populateAdmins() {

    Admin.find({})
      .then(admins => {
        if (!admins || admins.length < 1) {
          const admin = new Admin({
            username: adminName,
            password: adminPassword
          });

          admin.save()
            .then(() => console.log('Admin created'));
        }
      })
      .catch(err => console.log('Error: can\'t create admin'));

  },

  populateUsers() {
    User.find({})
      .then(users => {
        if (!users || users.length < 1) {
          const user = new User({
            cardNumber: userCardNumber,
            password: userPassword,
            firstname: "Елена",
            lastname: "Захарова",
            classes: []
           });

          user.save()
            .then(() => console.log('User created'));
        }
      })
      .catch(err => console.log('Error: can\'t create user'));
  }
}
