const Instructor = require('../models/instructor');
const _ = require('lodash');

module.exports = {
  getAll(req, res, next) {
    Instructor.find({})
      .then(instructors => res.json(instructors))
      .catch(next);
  },

  getOne(req, res, next) {
    const { id } = req.params;
    Instructor.findById(id)
      .then(instructor => res.json(instructor))
      .catch(next);
  },

  add(req, res, next) {
    const { firstname, lastname } = req.body;
    const instructor = new Instructor({ firstname, lastname });

    instructor.save()
      .then(instructor => res.json(instructor))
      .catch(next);
  },

  edit(req, res, next) {
    const { id } = req.params;
    const newProps = _.pick(req.body, ['firstname', 'lastname']);
    Instructor.findByIdAndUpdate( id, newProps)
      .then(() => Instructor.findById(id))
      .then(instructor => res.json(instructor))
      .catch(next);
  },

  delete(req, res, next) {
    const { id } = req.params;

    Instructor.findByIdAndRemove(id)
    .then(instructor => res.status(204).send(instructor))
    .catch(next);
  }
}
