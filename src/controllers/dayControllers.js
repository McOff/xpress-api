const _ = require('lodash');
const Day = require('../models/day');
const moment = require('moment');
moment.locale('ru');

module.exports = {

  getCurrentDays(req, res, next) {
    let today = moment().startOf('day');
    let todayAndTomorrow = setWeek(today).slice(0, 2);

    Day.find({ date: { $in: todayAndTomorrow } }).sort({ _id: 1 })
      .then(days => {
        res.json(days);
      })
      .catch(next);
  },

  getCurrentWeek(req, res, next) {
    let thisMonday = moment().startOf('week');
    let thisWeek = setWeek(thisMonday);

    Day.find({ date: { $in: thisWeek } }).sort({ _id: 1 })
      .then(week => {
        res.json(week);
      })
      .catch(next);
  },

  addNewWeek(req, res, next) {
    Day.find({})
      .sort({ _id: -1})
      .limit(1)
      .then(days => {

        if (days[0] && days[0].date) {
          let lastDay = days[0].date;
          let nextMonday = moment(lastDay).add(1, 'w').startOf('week');

          return setWeek(nextMonday);
        } else {
          let today = moment();
          let thisMonday = today.startOf('week');

          return setWeek(thisMonday);
        }
      })
      .then(weekArray => {
        let week = weekArray.map(day => {
          return {
            date: day,
            classes: []
          }
        })

        return Day.create(week);
      })
      .then(week => {
        res.json(week);
      })
      .catch(next);
  },

  copyWeek(req, res, next) {
    Day.find({})
      .sort({ _id: -1})
      .limit(1)
      .then(days => {
        if (!days[0] || !days[0].date) {
          res.json({error: 'There is no days in db.'})
        } else {
          let Monday = moment(days[0].date).startOf('week');
          let weekDays = setWeek(Monday);

          return Day.find({ date: { $in: weekDays } }).sort({ _id: 1 });

        }
      })
      .then(week => {

        let newWeek = week.map(day => {
          day.date = moment(day.date).add(1, 'w').toString();
          return day;
        });
        console.log(newWeek);

        return Day.create(newWeek);
      })
      .then(newWeek => {
        res.json(newWeek);
      })
      .catch(next);
  },

  addWorkout(req, res, next) {
    const { dayId, workout } = req.body;

    Day.findById(dayId)
      .then(day => {
        day.workouts.push(workout);
        return day.save();
      })
      .then(day => res.json(day))
      .catch(next);
  },

  deleteWorkout(req, res, next) {
    const { dayId, workoutId } = req.body;

    Day.findById(dayId)
      .then(day => {
        day.workouts = day.workouts.filter(workout => {
          workout.workout.toString() !== workoutId
        });
        
        return day.save();
      })
      .then(day => res.json(day))
      .catch(next);
  }
}

function setWeek(firstDay) {
  let weekArray = [];
  weekArray.push(firstDay.toString());

  for (let i = 1; i < 7; i++ ) {
    let newDay = firstDay.clone().add(i, 'd').toString();
    weekArray.push(newDay);
  }

  return weekArray;
}
