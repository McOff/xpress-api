const Workout = require('../models/workout');
const _ = require('lodash');

module.exports = {
  getAll(req, res, next) {
    Workout.find({})
      .then(workouts => res.json(workouts))
      .catch(next);
  },

  getOne(req, res, next) {
    const { id } = req.params;
    Workout.findById(id)
      .then(workout => res.json(workout))
      .catch(next);
  },

  add(req, res, next) {
    const { duration, title } = req.body;
    const workout = new Workout({ duration, title });

    workout.save()
      .then(workout => res.json(workout))
      .catch(next);
  },

  edit(req, res, next) {
    const { id } = req.params;
    const newProps = _.pick(req.body, ['title']);
    Workout.findByIdAndUpdate( id, newProps)
      .then(() => Workout.findById(id))
      .then(workout => res.json(workout))
      .catch(next);
  },

  delete(req, res, next) {
    const { id } = req.params;

    Workout.findByIdAndRemove(id)
    .then(workout => res.status(204).send(workout))
    .catch(next);
  }
}
