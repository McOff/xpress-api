const User = require('../models/user');
const jwt = require('jwt-simple');
const config = require('../config');

function tokenForAdmin(admin) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: admin._id, iat: timestamp }, config.secretAdmin);
}

exports.signin = (req, res, next) => {
  res.send({ token: tokenForAdmin(req.user) });
}
