const _ = require('lodash');
const jwt = require('jwt-simple');
const User = require('../models/user');
const config = require('../config');

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user._id, iat: timestamp }, config.secretUser);
}

module.exports = {
  signin(req, res, next) {
    res.send({ token: tokenForUser(req.user) });
  },

  getMe(req, res, next) {
    res.json(req.user);
  },

  editPassword(req, res, next) {

  }
}
