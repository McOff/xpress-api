const app = require('./app.js');
const config = require('./config');

app.listen( config.port, () => console.log(`App run on port ${config.port}`) );
