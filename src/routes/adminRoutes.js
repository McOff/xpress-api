const mongoose = require('mongoose');
const router = require('express').Router();
const passport = require('passport');
const AdminControllers = require('../controllers/adminControllers');

const requireAdminSignin = passport.authenticate('localAdmin', { session: false });

router.post('/signin', requireAdminSignin, AdminControllers.signin);

module.exports = router;
