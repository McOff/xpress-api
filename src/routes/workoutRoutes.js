const mongoose = require('mongoose');
const router = require('express').Router();
const passport = require('passport');
const WorkoutControllers = require('../controllers/workoutControllers');

const requireAdminAuth = passport.authenticate('jwtAdmin', { session: false });

router.get('/', WorkoutControllers.getAll);
router.post('/add', requireAdminAuth, WorkoutControllers.add);
router.get('/:id', requireAdminAuth, WorkoutControllers.getOne);
router.put('/:id', requireAdminAuth, WorkoutControllers.edit);
router.delete('/:id', requireAdminAuth, WorkoutControllers.delete);

module.exports = router;
