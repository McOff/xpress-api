const passportService = require('../services/passport');

const instructors = require('./instructorRoutes');
const users = require('./userRoutes');
const admins = require('./adminRoutes');
const workouts = require('./workoutRoutes');
const days = require('./dayRoutes');

module.exports = (app) => {
  app.use('/api/instructors', instructors);
  app.use('/api/users', users);
  app.use('/api/admins', admins);
  app.use('/api/workouts', workouts);
  app.use('/api/days', days);
};
