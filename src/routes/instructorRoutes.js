const mongoose = require('mongoose');
const router = require('express').Router();
const passport = require('passport');
const InstructorControllers = require('../controllers/instructorControllers');

const requireAdminAuth = passport.authenticate('jwtAdmin', { session: false });

router.get('/', InstructorControllers.getAll);
router.post('/add', requireAdminAuth, InstructorControllers.add);
router.get('/:id', InstructorControllers.getOne);
router.put('/:id', requireAdminAuth, InstructorControllers.edit);
router.delete('/:id', requireAdminAuth, InstructorControllers.delete);

module.exports = router;
