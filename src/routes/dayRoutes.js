const mongoose = require('mongoose');
const router = require('express').Router();
const passport = require('passport');
const DayControllers = require('../controllers/dayControllers');

const requireAdminAuth = passport.authenticate('jwtAdmin', { session: false });

router.get('/current', DayControllers.getCurrentDays);
router.get('/week/current', DayControllers.getCurrentWeek);
router.post('/week/add', requireAdminAuth, DayControllers.addNewWeek);
router.post('/week/copy', requireAdminAuth, DayControllers.copyWeek);
router.put('/day/add-workout', requireAdminAuth, DayControllers.addWorkout);
router.put('/day/delete-workout', requireAdminAuth, DayControllers.deleteWorkout);

module.exports = router;
