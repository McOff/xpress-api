const mongoose = require('mongoose');
const router = require('express').Router();
const passport = require('passport');
const UserControllers = require('../controllers/userControllers');

const requireUserSignin = passport.authenticate('localUser', { session: false });
const requireUserAuth = passport.authenticate('jwtUser', { session: false });

router.get('/me', requireUserAuth, UserControllers.getMe);
router.post('/signin', requireUserSignin, UserControllers.signin);


module.exports = router;
