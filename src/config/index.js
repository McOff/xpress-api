const { secretAdmin, secretUser } = require('./config');
const { adminName, adminPassword, userCardNumber, userPassword } = require('./config');


module.exports = {
  port: process.env.PORT || 3000,
  bodyLimit: "1024kb",
  "mongoUrl": "mongodb://localhost:27017/xpress",
  secretAdmin, secretUser,
  adminName, adminPassword, userCardNumber, userPassword
}
